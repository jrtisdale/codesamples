using Extranet.Data.Models.Bases;

namespace Extranet.Data.Models
{
    public class Status : DbObject
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}