using System;
using Extranet.Data.Models.Bases;

namespace Extranet.Data.Models
{
    public class TimeLog : DatedDbObject
    {
        public TimeLog()
        {
            
        }

        public int ModifiedByUserID { get; set; }
        public int? UserID { get; set; }
        public User User { get; set; }

        public DateTime? BeginTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int Minutes { get; set; }

        public string Comment { get; set; }

        public int? TaskRequestID { get; set; }
        public int? ProjectID { get; set; }

        public TaskRequest TaskRequest { get; set; }
        public Project Project { get; set; }
    }
}