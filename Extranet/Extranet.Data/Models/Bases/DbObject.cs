namespace Extranet.Data.Models.Bases
{
    public abstract class DbObject
    {
        public int ID { get; set; }
    }
}