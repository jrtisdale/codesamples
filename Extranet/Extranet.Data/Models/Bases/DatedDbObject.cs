using System;

namespace Extranet.Data.Models.Bases
{
    public abstract class DatedDbObject : DbObject
    {
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
    }
}