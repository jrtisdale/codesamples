using System;
using System.Collections.Generic;
using Extranet.Data.Models.Bases;

namespace Extranet.Data.Models
{
    public class TaskRequest : DatedDbObject
    {
        public TaskRequest()
        {
            this.Comments = new List<Comment>();
            this.TimeLogs = new List<TimeLog>();
        }

        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? DateDue { get; set; }
        public int? CreatedByUserID { get; set; }
        public int? AssignedToUserID { get; set; }
        public int? StatusID { get; set; }
        public bool IsComplete { get; set; }
        public virtual User CreatedByUser { get; set; }
        public virtual User AssignedToUser { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<TimeLog> TimeLogs { get; set; }
        public int? ProjectID { get; set; }
        public virtual Project Project { get; set; }
    }
}
