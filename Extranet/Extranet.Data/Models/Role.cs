using System;
using System.Collections.Generic;
using Extranet.Data.Models.Bases;

namespace Extranet.Data.Models
{
    public class Role : DbObject
    {
        public Role()
        {
            this.Permissions = new List<Permission>();
            this.Users = new List<User>();
        }

        public string Name { get; set; }
        public virtual ICollection<Permission> Permissions { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
