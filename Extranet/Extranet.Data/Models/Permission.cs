using System;
using System.Collections.Generic;
using Extranet.Data.Models.Bases;

namespace Extranet.Data.Models
{
    public class Permission : DbObject
    {
        public Permission()
        {
            this.Roles = new List<Role>();
        }

        public string Name { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
    }
}
