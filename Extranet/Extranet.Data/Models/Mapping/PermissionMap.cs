using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Extranet.Data.Models.Mapping
{
    public class PermissionMap : EntityTypeConfiguration<Permission>
    {
        public PermissionMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Permissions");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");

            // Relationships
            this.HasMany(t => t.Roles)
                .WithMany(t => t.Permissions)
                .Map(m =>
                    {
                        m.ToTable("RolePermissions");
                        m.MapLeftKey("PermissionID");
                        m.MapRightKey("RoleID");
                    });


        }
    }
}
