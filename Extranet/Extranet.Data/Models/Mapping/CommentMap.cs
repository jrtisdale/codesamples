using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Extranet.Data.Models.Mapping
{
    public class CommentMap : EntityTypeConfiguration<Comment>
    {
        public CommentMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Title)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Comments");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
            this.Property(t => t.DateLastModified).HasColumnName("DateLastModified");
            this.Property(t => t.CreatedByUserID).HasColumnName("CreatedByUserID");
            this.Property(t => t.EditedByUserID).HasColumnName("EditedByUserID");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Text).HasColumnName("Text");

            // Relationships
            this.HasMany(t => t.TaskRequests)
                .WithMany(t => t.Comments)
                .Map(m =>
                    {
                        m.ToTable("TaskRequestComments");
                        m.MapLeftKey("CommentID");
                        m.MapRightKey("TaskRequestID");
                    });

            this.HasOptional(t => t.CreatedByUser)
                .WithMany(t => t.CreatedComments)
                .HasForeignKey(d => d.CreatedByUserID);
            this.HasOptional(t => t.EditedByUser)
                .WithMany(t => t.EditedComments)
                .HasForeignKey(d => d.EditedByUserID);

        }
    }
}
