using System.Data.Entity.ModelConfiguration;

namespace Extranet.Data.Models.Mapping
{
    public class TimeLogMap : EntityTypeConfiguration<TimeLog>
    {
        public TimeLogMap()
        {

            this.ToTable("TimeLogs");

            this.HasKey(t => t.ID);
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
            this.Property(t => t.DateLastModified).HasColumnName("DateLastModified");
            this.Property(t => t.ModifiedByUserID);

            this.Property(t => t.BeginTime);
            this.Property(t => t.EndTime);
            this.Property(t => t.Minutes);

            this.Property(t => t.Comment);

            this.Property(t => t.TaskRequestID);
            this.Property(t => t.ProjectID);
            this.Property(t => t.UserID);
        }
    }
}