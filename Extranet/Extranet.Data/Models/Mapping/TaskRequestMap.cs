using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Extranet.Data.Models.Mapping
{
    public class TaskRequestMap : EntityTypeConfiguration<TaskRequest>
    {
        public TaskRequestMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Title)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("TaskRequests");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
            this.Property(t => t.DateLastModified).HasColumnName("DateLastModified");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.DateDue).HasColumnName("DateDue");
            this.Property(t => t.CreatedByUserID).HasColumnName("CreatedByUserID");
            this.Property(t => t.AssignedToUserID).HasColumnName("AssignedToUserID");
            this.Property(t => t.StatusID).HasColumnName("StatusID");
            this.Property(t => t.IsComplete).HasColumnName("IsComplete");

            this.HasOptional(t => t.Status)
                .WithMany()
                .HasForeignKey(a => a.StatusID);

            // Relationships
            this.HasOptional(t => t.AssignedToUser)
                .WithMany(t => t.AssignedTasks)
                .HasForeignKey(d => d.AssignedToUserID);
            this.HasOptional(t => t.CreatedByUser)
                .WithMany(t => t.CreatedTasks)
                .HasForeignKey(d => d.CreatedByUserID);
            this.HasOptional(t => t.Project)
                .WithMany(t => t.TaskRequests)
                .HasForeignKey(d => d.ProjectID);

            this.HasMany(p => p.TimeLogs)
                .WithOptional(p => p.TaskRequest)
                .HasForeignKey(p => p.TaskRequestID);

        }
    }
}
