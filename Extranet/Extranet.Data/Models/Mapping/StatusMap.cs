using System.Data.Entity.ModelConfiguration;

namespace Extranet.Data.Models.Mapping
{
    public class StatusMap : EntityTypeConfiguration<Status>
    {
        public StatusMap()
        {
            this.HasKey(t => t.ID);
            this.Property(t => t.Name).HasMaxLength(50);
            this.ToTable("Statuses");
        }
    }
}