using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Extranet.Data.Models.Mapping
{
    public class ProjectMap : EntityTypeConfiguration<Project>
    {
        public ProjectMap()
        {
            this.ToTable("Projects");
            this.HasKey(t => t.ID);
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
            this.Property(t => t.DateLastModified).HasColumnName("DateLastModified");

            this.Property(t => t.Name).HasMaxLength(100);
            this.Property(t => t.JiraID).HasMaxLength(50);

            this.HasMany(p => p.TimeLogs)
                .WithOptional(a => a.Project)
                .HasForeignKey(p => p.ProjectID);

            this.HasMany(p => p.TimeLogs)
                .WithOptional(a => a.Project)
                .HasForeignKey(a => a.ProjectID);
        }
    }
}