using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Extranet.Data.Models.Mapping;

namespace Extranet.Data.Models
{
    public class ExtranetContext : DbContext
    {
        static ExtranetContext()
        {
            Database.SetInitializer<ExtranetContext>(null);
        }

		public ExtranetContext()
			: base("Name=ExtranetContext")
		{
		}

        public DbSet<Comment> Comments { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<TaskRequest> TaskRequests { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<TimeLog> TimeLogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CommentMap());
            modelBuilder.Configurations.Add(new PermissionMap());
            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new TaskRequestMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new StatusMap());
            modelBuilder.Configurations.Add(new ProjectMap());
            modelBuilder.Configurations.Add(new TimeLogMap());
        }
    }
}
