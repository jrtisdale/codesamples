using System.Collections.Generic;
using Extranet.Data.Models.Bases;

namespace Extranet.Data.Models
{
    public class Project : DatedDbObject
    {
        public Project()
        {
            this.TimeLogs = new List<TimeLog>();
            this.TaskRequests = new List<TaskRequest>();
        }

        public string Name { get; set; }
        public string JiraID { get; set; }

        public virtual ICollection<TimeLog> TimeLogs { get; set; }
        public virtual ICollection<TaskRequest> TaskRequests { get; set; } 
    }
}