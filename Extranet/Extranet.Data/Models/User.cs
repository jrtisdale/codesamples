using System.Collections.Generic;
using Extranet.Data.Models.Bases;

namespace Extranet.Data.Models
{
    public class User : DbObject
    {
        public User()
        {
            this.CreatedComments = new List<Comment>();
            this.EditedComments = new List<Comment>();
            this.AssignedTasks = new List<TaskRequest>();
            this.CreatedTasks = new List<TaskRequest>();
            this.Roles = new List<Role>();
            this.TimeLogs = new List<TimeLog>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public virtual ICollection<Comment> CreatedComments { get; set; }
        public virtual ICollection<Comment> EditedComments { get; set; }
        public virtual ICollection<TaskRequest> AssignedTasks { get; set; }
        public virtual ICollection<TaskRequest> CreatedTasks { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
        public virtual ICollection<TimeLog> TimeLogs { get; set; } 
    }
}
