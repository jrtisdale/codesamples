using System;
using System.Collections.Generic;
using Extranet.Data.Models.Bases;

namespace Extranet.Data.Models
{
    public class Comment : DatedDbObject
    {
        public Comment()
        {
            this.TaskRequests = new List<TaskRequest>();
        }

        public int? CreatedByUserID { get; set; }
        public int? EditedByUserID { get; set; }

        public string Title { get; set; }
        public string Text { get; set; }

        public virtual User CreatedByUser { get; set; }
        public virtual User EditedByUser { get; set; }

        public virtual ICollection<TaskRequest> TaskRequests { get; set; }
    }
}
