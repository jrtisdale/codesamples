﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Extranet.Data.Access.Repositories;
using Extranet.Data.Models;

namespace Extranet.Data.Access
{
    public class TaskRequestService
    {
        private ExtranetContext _context;
        private UserRepository users;
        private TaskRequestRepository taskRequests;
        private CommentRepository comments;

        public TaskRequestService(ExtranetContext context)
        {
            _context = context;
            users = new UserRepository(context);
            taskRequests = new TaskRequestRepository(context);
            comments = new CommentRepository(context);
        }

        public Comment AddComment(Comment comment)
        {
            return comments.Save(comment);
        }

    }
}
