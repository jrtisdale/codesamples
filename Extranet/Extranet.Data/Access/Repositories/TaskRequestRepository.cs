﻿using System.Collections.Generic;
using System.Linq;
using Extranet.Data.Access.Repositories.Abstract;
using Extranet.Data.JSON;
using Extranet.Data.Models;

namespace Extranet.Data.Access.Repositories
{
    public class TaskRequestRepository : RepositoryBase<TaskRequest>
    {
        public TaskRequestRepository() : base(new ExtranetContext())
        {
            
        }

        public TaskRequestRepository(ExtranetContext context) : base(context)
        {
            
        }

        public new IQueryable<TaskRequest> Get()
        {
            return _context.TaskRequests.Include("AssignedToUser").Include("CreatedByUser");
        }

        public IQueryable<TaskRequest> GetAssignedForUser(int id)
        {
            return _context.TaskRequests
                            .Include("AssignedToUser")
                            .Include("CreatedByUser")
                            .Where(a => a.AssignedToUserID == id);
        }

        public new TaskRequest Find(int id)
        {
            return _context.TaskRequests
                                .Include("AssignedToUser")
                                .Include("CreatedByUser")
                                .Include("Comments")
                                .FirstOrDefault(a => a.ID == id);
        }

        public TaskRequestJson GetJSON(int id)
        {
            return Get(w => w.ID == id).ToList().Select(a => TaskRequestJson.MapFrom(a)).FirstOrDefault();
        }

        public List<TaskRequestJson> GetJSONForUser(int userID)
        {
            return Get(w => w.AssignedToUserID == userID).ToList().Select(a => TaskRequestJson.MapFrom(a)).ToList();
        }

        public List<TaskRequestJson> GetJSONForProject(int projectID)
        {
            return Get(w => w.ProjectID == projectID).ToList().Select(a => TaskRequestJson.MapFrom(a)).ToList();
        }
    }
}
