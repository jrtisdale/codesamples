using Extranet.Data.Access.Repositories.Abstract;
using Extranet.Data.Models;

namespace Extranet.Data.Access.Repositories
{
    public class CommentRepository : RepositoryBase<Comment>
    {
        public CommentRepository()
            : base(new ExtranetContext())
        {

        }

        public CommentRepository(ExtranetContext context)
            : base(context)
        {

        }
    }
}