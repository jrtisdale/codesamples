using System.Linq;
using Extranet.Data.Access.Repositories.Abstract;
using Extranet.Data.Models;

namespace Extranet.Data.Access.Repositories
{
    public class UserRepository : RepositoryBase<User>
    {
        public UserRepository()
            : base(new ExtranetContext())
        {

        }

        public UserRepository(ExtranetContext context)
            : base(context)
        {

        }

        public new IQueryable<User> Get()
        {
            var users = _context.Users.Include("AssignedTasks").Include("CreatedTasks").Include("Roles");
            return users;
        }

        public new User Find(int id)
        {
            var user = _context.Users
                .Include("AssignedTasks")
                .Include("CreatedTasks")
                .Include("Roles")
                .FirstOrDefault(a => a.ID == id);

            return user;
        }
    }
}