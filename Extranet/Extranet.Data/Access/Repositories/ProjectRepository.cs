using System.Collections.Generic;
using System.Linq;
using Extranet.Data.Access.Repositories.Abstract;
using Extranet.Data.JSON;
using Extranet.Data.Models;

namespace Extranet.Data.Access.Repositories
{
    public class ProjectRepository : RepositoryBase<Project>
    {
        public ProjectRepository() : base(new ExtranetContext())
        {
            
        }

        public ProjectRepository(ExtranetContext context) : base(context)
        {
            
        }

        public List<ProjectJson> GetJSON(int userID)
        {
            return Get().ToList().Select(a => ProjectJson.MapFrom(a)).ToList();
        }
    }
}