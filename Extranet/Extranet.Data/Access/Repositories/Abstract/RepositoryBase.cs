using System;
using System.Collections.Generic;
using System.Linq;
using Extranet.Data.Access.Queries.Abstract;
using Extranet.Data.Models;
using Extranet.Data.Models.Bases;

namespace Extranet.Data.Access.Repositories.Abstract
{
    public abstract class RepositoryBase<T> : IRepository<T> where T : DbObject
    {
        protected ExtranetContext _context;

        protected RepositoryBase(ExtranetContext context)
        {
            _context = context;
        }

        public T Find(int id)
        {
            var query = new FindQuery<T>(_context);
            return  query.Execute(id);
        }

        public IQueryable<T> Get(Func<T, bool> whereClause)
        {
            var query = new GetQuery<T>(_context);
            return query.Execute(whereClause);
        }

        public IQueryable<T> Get()
        {
            var query = new GetQuery<T>(_context);
            return query.Execute();
        }

        public T Save(T toSave)
        {
            var query = new SaveQuery<T>(_context);
            return query.Execute(toSave);
        }

        public void Save(IEnumerable<T> toSave)
        {
            var query = new BulkSaveQuery<T>(_context);
            query.Execute(toSave);
        }

        public void Delete(T toDelete)
        {
            var query = new DeleteQuery<T>(_context);
            query.Execute(toDelete);
        }

        public void Delete(IEnumerable<T> toDelete)
        {
            var query = new BulkDeleteQuery<T>(_context);
            query.Execute(toDelete);
        }
    }
}