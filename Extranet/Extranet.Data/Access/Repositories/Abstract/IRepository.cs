﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extranet.Data.Access.Repositories.Abstract
{
    public interface IRepository<T>
    {
        T Find(int id);

        IQueryable<T> Get(Func<T, bool> whereClause);
        IQueryable<T> Get();

        T Save(T toSave);
        void Save(IEnumerable<T> toSave);

        void Delete(T toDelete);
        void Delete(IEnumerable<T> toDelete);
    }
}
