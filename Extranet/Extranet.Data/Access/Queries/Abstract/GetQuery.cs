using System;
using System.Linq;
using Extranet.Data.Models;

namespace Extranet.Data.Access.Queries.Abstract
{
    public class GetQuery<TType> : QueryActionBase, IQuery<Func<TType, bool>, IQueryable<TType>>
    {
        public GetQuery(ExtranetContext context)
            : base(context)
        {

        }

        public IQueryable<TType> Execute(Func<TType, bool> context = null)
        {
            var set = _context.Set(typeof(TType));
            var typedSet = set.OfType<TType>();

            if (typedSet == null)
                throw new Exception("This is not an Entity type, and therefore cannot be retrieved from the database.");

            if (context != null)
            {
                return typedSet.Where(context).AsQueryable();
            }
            else
            {
                return typedSet.AsQueryable();
            }
        }
    }
}