﻿using System;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Extranet.Data.Models;
using Extranet.Data.Models.Bases;

namespace Extranet.Data.Access.Queries.Abstract
{
    public class SaveQuery<TType> : QueryActionBase, IQuery<DbObject, TType> where TType : class
    {
        public SaveQuery(ExtranetContext context)
            : base(context)
        {

        }

        public TType Execute(DbObject context)
        {
            var set = _context.Set(typeof(TType));

            if (set == null)
                throw new Exception("This is not an Entity type, and therefore cannot be retrieved from the database.");

            if (context.ID < 1)
            {
                set.Add(context);
            }
            else
            {
                Debug.WriteLine(_context.Entry(context).State);

                if (!EntityIsAttached(context as TType))
                    _context.Entry(context).State = EntityState.Modified;
            }

            _context.SaveChanges();
            return (TType)set.Find(context.ID);
        }
    }
}
