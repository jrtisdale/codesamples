using System;
using Extranet.Data.Models;
using Extranet.Data.Models.Bases;

namespace Extranet.Data.Access.Queries.Abstract
{
    public class DeleteQuery<TType> : QueryActionBase, IQuery<DbObject, bool>
    {
        public DeleteQuery(ExtranetContext context)
            : base(context)
        {

        }

        public bool Execute(DbObject context)
        {
            var set = _context.Set(typeof(TType));

            if (set == null)
                throw new Exception("This is not an Entity type, and therefore cannot be retrieved from the database.");

            if (context.ID < 1)
            {
                return false;
            }
            else
            {
                set.Remove(context);
            }

            _context.SaveChanges();

            return set.Find(context.ID) == null;
        }
    }
}