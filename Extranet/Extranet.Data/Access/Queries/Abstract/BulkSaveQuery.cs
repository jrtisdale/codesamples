using System;
using System.Collections.Generic;
using System.Data;
using Extranet.Data.Models;
using Extranet.Data.Models.Bases;

namespace Extranet.Data.Access.Queries.Abstract
{
    public class BulkSaveQuery<TType> : QueryActionBase, IQuery<IEnumerable<DbObject>, bool>
    {
        public BulkSaveQuery(ExtranetContext context)
            : base(context)
        {

        }

        public bool Execute(IEnumerable<DbObject> context)
        {
            var set = _context.Set(typeof(TType));

            if (set == null)
                throw new Exception("This is not an Entity type, and therefore cannot be retrieved from the database.");

            foreach (var item in context)
            {
                if (item.ID < 1)
                {
                    set.Add(item);
                }
                else
                {
                    _context.Entry(item).State = EntityState.Modified;
                }
            }

            _context.SaveChanges();
            return true;
        }
    }
}