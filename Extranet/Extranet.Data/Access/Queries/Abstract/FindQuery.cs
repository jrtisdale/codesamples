using System;
using Extranet.Data.Models;

namespace Extranet.Data.Access.Queries.Abstract
{
    public class FindQuery<TType> : QueryActionBase, IQuery<int, TType>
    {
        public FindQuery(ExtranetContext context)
            : base(context)
        {

        }

        public TType Execute(int context)
        {
            var set = _context.Set(typeof(TType));

            if (set == null)
                throw new Exception("This is not an Entity type, and therefore cannot be retrieved from the database.");

            return (TType)set.Find(context);
        }
    }
}