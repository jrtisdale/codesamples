using System;
using System.Collections.Generic;
using System.Linq;
using Extranet.Data.Models;
using Extranet.Data.Models.Bases;

namespace Extranet.Data.Access.Queries.Abstract
{
    public class BulkDeleteQuery<TType> : QueryActionBase, IQuery<IEnumerable<DbObject>, Dictionary<DbObject, bool>>
    {
        public BulkDeleteQuery(ExtranetContext context)
            : base(context)
        {

        }

        public Dictionary<DbObject, bool> Execute(IEnumerable<DbObject> context)
        {
            var set = _context.Set(typeof(TType));
            var typedSet = set.OfType<TType>();

            if (set == null)
                throw new Exception("This is not an Entity type, and therefore cannot be retrieved from the database.");

            var status = new Dictionary<DbObject, bool>();
            foreach (var item in context)
            {
                if (item.ID < 1)
                {
                    status.Add(item, true);
                }
                else
                {
                    set.Remove(item);
                    status.Add(item, true);
                }
            }

            _context.SaveChanges();

            return status;
        }
    }
}