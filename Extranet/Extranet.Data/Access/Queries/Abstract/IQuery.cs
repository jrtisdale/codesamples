namespace Extranet.Data.Access.Queries.Abstract
{
    public interface IQuery<in TContext, out TResult>
    {
        TResult Execute(TContext context);
    }

    public interface ICustomQuery<in TContext, ExtraData, out TResult>
    {
        TResult Execute(TContext context, ExtraData extraData);
    }
}