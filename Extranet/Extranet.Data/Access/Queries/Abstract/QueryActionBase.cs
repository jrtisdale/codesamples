using Extranet.Data.Models;

namespace Extranet.Data.Access.Queries.Abstract
{
    public abstract class QueryActionBase
    {
        protected ExtranetContext _context;

        public QueryActionBase(ExtranetContext context)
        {
            _context = context;
        }

        public QueryActionBase()
        {
            _context = new ExtranetContext();
        }

        public bool EntityIsAttached<T>(T entity) where T : class
        {
            return _context.Set(typeof(T)).Local.Contains(entity);
            //return _context.Set<T>().Local.Any(e => e == entity);
        }
    }
}