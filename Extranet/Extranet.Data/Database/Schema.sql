﻿USE [Extranet]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 09/28/2012 17:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Email] [nvarchar](100) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Users] ON
INSERT [dbo].[Users] ([ID], [FirstName], [LastName], [Email]) VALUES (1, N'Justin', N'Tisdale', N'justin.tisdale@live.com')
SET IDENTITY_INSERT [dbo].[Users] OFF
/****** Object:  Table [dbo].[Projects]    Script Date: 09/28/2012 17:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Projects](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateLastModified] [datetime] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[JiraID] [nvarchar](50) NULL,
 CONSTRAINT [PK_Projects] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Projects] ON
INSERT [dbo].[Projects] ([ID], [DateCreated], [DateLastModified], [Name], [JiraID]) VALUES (1, CAST(0x0000A0DA0118BCA6 AS DateTime), CAST(0x0000A0DA0118BCA6 AS DateTime), N'Example Project', NULL)
SET IDENTITY_INSERT [dbo].[Projects] OFF
/****** Object:  Table [dbo].[Permissions]    Script Date: 09/28/2012 17:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permissions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_Permissions] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Statuses]    Script Date: 09/28/2012 17:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Statuses](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Statuses] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Statuses] ON
INSERT [dbo].[Statuses] ([ID], [Name]) VALUES (1, N'Active')
INSERT [dbo].[Statuses] ([ID], [Name]) VALUES (2, N'On Hold')
INSERT [dbo].[Statuses] ([ID], [Name]) VALUES (3, N'Complete')
INSERT [dbo].[Statuses] ([ID], [Name]) VALUES (4, N'Archived')
SET IDENTITY_INSERT [dbo].[Statuses] OFF
/****** Object:  Table [dbo].[Roles]    Script Date: 09/28/2012 17:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Roles] ON
INSERT [dbo].[Roles] ([ID], [Name]) VALUES (1, N'Administrator')
SET IDENTITY_INSERT [dbo].[Roles] OFF
/****** Object:  Table [dbo].[RolePermissions]    Script Date: 09/28/2012 17:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RolePermissions](
	[RoleID] [int] NOT NULL,
	[PermissionID] [int] NOT NULL,
 CONSTRAINT [PK_RolePermissions] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC,
	[PermissionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comments]    Script Date: 09/28/2012 17:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comments](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateLastModified] [datetime] NOT NULL,
	[CreatedByUserID] [int] NULL,
	[EditedByUserID] [int] NULL,
	[Title] [varchar](200) NULL,
	[Text] [varchar](max) NULL,
 CONSTRAINT [PK_Comments] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TaskRequests]    Script Date: 09/28/2012 17:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaskRequests](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateLastModified] [datetime] NOT NULL,
	[Title] [nvarchar](150) NULL,
	[Description] [nvarchar](MAX) NULL,
	[DateDue] [datetime] NULL,
	[CreatedByUserID] [int] NULL,
	[AssignedToUserID] [int] NULL,
	[StatusID] [int] NULL,
	[IsComplete] [bit] NOT NULL,
	[ProjectID] [int] NULL,
 CONSTRAINT [PK_TaskRequests] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[TaskRequests] ON
INSERT [dbo].[TaskRequests] ([ID], [DateCreated], [DateLastModified], [Title], [DateDue], [CreatedByUserID], [AssignedToUserID], [StatusID], [IsComplete], [ProjectID]) VALUES (1, CAST(0x0000A0CF00BACC89 AS DateTime), CAST(0x0000A0CF00BACC89 AS DateTime), N'TR1', NULL, 1, 1, 1, 0, 1)
SET IDENTITY_INSERT [dbo].[TaskRequests] OFF
/****** Object:  Table [dbo].[UserRoles]    Script Date: 09/28/2012 17:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoles](
	[UserID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
 CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[RoleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[UserRoles] ([UserID], [RoleID]) VALUES (1, 1)
/****** Object:  Table [dbo].[TimeLogs]    Script Date: 09/28/2012 17:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimeLogs](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateLastModified] [datetime] NOT NULL,
	[ModifiedByUserID] [int] NULL,
	[UserID] [int] NULL,
	[BeginTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Minutes] [int] NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[TaskRequestID] [int] NULL,
	[ProjectID] [int] NULL,
 CONSTRAINT [PK_TimeLogs] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TaskRequestComments]    Script Date: 09/28/2012 17:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaskRequestComments](
	[TaskRequestID] [int] NOT NULL,
	[CommentID] [int] NOT NULL,
 CONSTRAINT [PK_TaskRequestComments] PRIMARY KEY CLUSTERED 
(
	[TaskRequestID] ASC,
	[CommentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_Projects_DateCreated]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[Projects] ADD  CONSTRAINT [DF_Projects_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF_Projects_DateLastModified]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[Projects] ADD  CONSTRAINT [DF_Projects_DateLastModified]  DEFAULT (getdate()) FOR [DateLastModified]
GO
/****** Object:  Default [DF_Comments_DateCreated]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[Comments] ADD  CONSTRAINT [DF_Comments_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF_Comments_DateLastModified]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[Comments] ADD  CONSTRAINT [DF_Comments_DateLastModified]  DEFAULT (getdate()) FOR [DateLastModified]
GO
/****** Object:  Default [DF_TaskRequests_DateCreated]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[TaskRequests] ADD  CONSTRAINT [DF_TaskRequests_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF_TaskRequests_DateLastModified]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[TaskRequests] ADD  CONSTRAINT [DF_TaskRequests_DateLastModified]  DEFAULT (getdate()) FOR [DateLastModified]
GO
/****** Object:  Default [DF_TaskRequests_IsComplete]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[TaskRequests] ADD  CONSTRAINT [DF_TaskRequests_IsComplete]  DEFAULT ((0)) FOR [IsComplete]
GO
/****** Object:  Default [DF_TimeLogs_Minutes]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[TimeLogs] ADD  CONSTRAINT [DF_TimeLogs_Minutes]  DEFAULT ((0)) FOR [Minutes]
GO
/****** Object:  ForeignKey [FK_RolePermissions_Permissions]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[RolePermissions]  WITH CHECK ADD  CONSTRAINT [FK_RolePermissions_Permissions] FOREIGN KEY([PermissionID])
REFERENCES [dbo].[Permissions] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RolePermissions] CHECK CONSTRAINT [FK_RolePermissions_Permissions]
GO
/****** Object:  ForeignKey [FK_RolePermissions_Roles]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[RolePermissions]  WITH CHECK ADD  CONSTRAINT [FK_RolePermissions_Roles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Roles] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RolePermissions] CHECK CONSTRAINT [FK_RolePermissions_Roles]
GO
/****** Object:  ForeignKey [FK_Comments_CreatedByUser]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD  CONSTRAINT [FK_Comments_CreatedByUser] FOREIGN KEY([CreatedByUserID])
REFERENCES [dbo].[Users] ([ID])
GO
ALTER TABLE [dbo].[Comments] CHECK CONSTRAINT [FK_Comments_CreatedByUser]
GO
/****** Object:  ForeignKey [FK_Comments_EditedByUser]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD  CONSTRAINT [FK_Comments_EditedByUser] FOREIGN KEY([EditedByUserID])
REFERENCES [dbo].[Users] ([ID])
GO
ALTER TABLE [dbo].[Comments] CHECK CONSTRAINT [FK_Comments_EditedByUser]
GO
/****** Object:  ForeignKey [FK_TaskRequests_AssignedToUser]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[TaskRequests]  WITH CHECK ADD  CONSTRAINT [FK_TaskRequests_AssignedToUser] FOREIGN KEY([AssignedToUserID])
REFERENCES [dbo].[Users] ([ID])
GO
ALTER TABLE [dbo].[TaskRequests] CHECK CONSTRAINT [FK_TaskRequests_AssignedToUser]
GO
/****** Object:  ForeignKey [FK_TaskRequests_CreatedByUser]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[TaskRequests]  WITH CHECK ADD  CONSTRAINT [FK_TaskRequests_CreatedByUser] FOREIGN KEY([CreatedByUserID])
REFERENCES [dbo].[Users] ([ID])
GO
ALTER TABLE [dbo].[TaskRequests] CHECK CONSTRAINT [FK_TaskRequests_CreatedByUser]
GO
/****** Object:  ForeignKey [FK_TaskRequests_Projects]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[TaskRequests]  WITH CHECK ADD  CONSTRAINT [FK_TaskRequests_Projects] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[Projects] ([ID])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[TaskRequests] CHECK CONSTRAINT [FK_TaskRequests_Projects]
GO
/****** Object:  ForeignKey [FK_TaskRequests_Statuses]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[TaskRequests]  WITH CHECK ADD  CONSTRAINT [FK_TaskRequests_Statuses] FOREIGN KEY([StatusID])
REFERENCES [dbo].[Statuses] ([ID])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[TaskRequests] CHECK CONSTRAINT [FK_TaskRequests_Statuses]
GO
/****** Object:  ForeignKey [FK_UserRoles_Roles]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Roles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Roles] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_Roles]
GO
/****** Object:  ForeignKey [FK_UserRoles_Users]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Users] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_Users]
GO
/****** Object:  ForeignKey [FK_TimeLogs_Projects]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[TimeLogs]  WITH CHECK ADD  CONSTRAINT [FK_TimeLogs_Projects] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[Projects] ([ID])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[TimeLogs] CHECK CONSTRAINT [FK_TimeLogs_Projects]
GO
/****** Object:  ForeignKey [FK_TimeLogs_TaskRequests]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[TimeLogs]  WITH CHECK ADD  CONSTRAINT [FK_TimeLogs_TaskRequests] FOREIGN KEY([TaskRequestID])
REFERENCES [dbo].[TaskRequests] ([ID])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[TimeLogs] CHECK CONSTRAINT [FK_TimeLogs_TaskRequests]
GO
/****** Object:  ForeignKey [FK_TimeLogs_Users]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[TimeLogs]  WITH CHECK ADD  CONSTRAINT [FK_TimeLogs_Users] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([ID])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[TimeLogs] CHECK CONSTRAINT [FK_TimeLogs_Users]
GO
/****** Object:  ForeignKey [FK_TaskRequestComments_Comments]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[TaskRequestComments]  WITH CHECK ADD  CONSTRAINT [FK_TaskRequestComments_Comments] FOREIGN KEY([CommentID])
REFERENCES [dbo].[Comments] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TaskRequestComments] CHECK CONSTRAINT [FK_TaskRequestComments_Comments]
GO
/****** Object:  ForeignKey [FK_TaskRequestComments_TaskRequests]    Script Date: 09/28/2012 17:19:42 ******/
ALTER TABLE [dbo].[TaskRequestComments]  WITH CHECK ADD  CONSTRAINT [FK_TaskRequestComments_TaskRequests] FOREIGN KEY([TaskRequestID])
REFERENCES [dbo].[TaskRequests] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TaskRequestComments] CHECK CONSTRAINT [FK_TaskRequestComments_TaskRequests]
GO
