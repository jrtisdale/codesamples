﻿using System;
using System.Linq;
using System.Text;
using AutoMapper;
using Extranet.Data.Models;

namespace Extranet.Data.JSON
{
    public static class JsonMappingConfig
    {
        public static void Map()
        {
            Mapper.CreateMap<TaskRequest, TaskRequestJson>()
                .ForMember(p => p.DateCreated, a => a.MapFrom(o => o.DateCreated == null ? string.Empty : o.DateCreated.ToString("yyyy-MM-dd h:mm tt")))
                .ForMember(p => p.DateLastModified, a => a.MapFrom(o => o.DateLastModified == null ? string.Empty : o.DateLastModified.ToString("yyyy-MM-dd h:mm tt")))
                .ForMember(p => p.DateDue, a => a.MapFrom(o => (o.DateDue ?? o.DateCreated.AddDays(3)).ToString("yyyy-MM-dd h:mm tt")))
                .ForMember(p => p.IsComplete, a => a.MapFrom(o => o.IsComplete))
                .ForMember(p => p.StatusID, a => a.MapFrom(o => o.StatusID))
                .ForMember(p => p.Description, a => a.MapFrom(o => ToBase64(o.Description)))
                .ForMember(p => p.Title, a => a.MapFrom(o => o.Title))
                .ForMember(p => p.ID, a => a.MapFrom(o => o.ID))
                .ForMember(p => p.AssignedToUserID, a => a.MapFrom(o => o.AssignedToUserID))
                .ForMember(p => p.CreatedByUserID, a => a.MapFrom(o => o.CreatedByUserID))
                .ForMember(p => p.CreatedByUser, a => a.MapFrom(o => o.CreatedByUser))
                .ForMember(p => p.AssignedToUser, a => a.MapFrom(o => o.AssignedToUser))
                .ForMember(p => p.CreatedByUser, a => a.MapFrom(o => o.CreatedByUser))
                .ForMember(p => p.Comments, a => a.Ignore())
                .ForMember(p => p.Status, a => a.MapFrom(o => o.Status))
                .ForMember(p => p.ProjectID, a => a.MapFrom(o => o.ProjectID))
                .ForMember(p => p.Project, a => a.MapFrom(o => o.Project))
                .ForMember(p => p.TimeString, a => a.MapFrom(o => o.TimeLogs.Sum(d => d.Minutes)));

            Mapper.CreateMap<User, UserJson>()
                .ForMember(p => p.ID, a => a.MapFrom(o => o.ID))
                .ForMember(p => p.FirstName, a => a.MapFrom(o => o.FirstName))
                .ForMember(p => p.LastName, a => a.MapFrom(o => o.LastName))
                .ForMember(p => p.Email, a => a.MapFrom(o => o.Email))
                //.ForMember(p => p.Roles, a => a.Ignore())
                .ForMember(p => p.AssignedTasks, a => a.MapFrom(o => o.AssignedTasks))
                .ForMember(p => p.CreatedTasks, a => a.MapFrom(o => o.CreatedTasks))
                .ForMember(p => p.CreatedComments, a => a.MapFrom(o => o.CreatedComments))
                .ForMember(p => p.EditedComments, a => a.MapFrom(o => o.EditedComments));

            Mapper.CreateMap<Comment, CommentJson>()
                .ForMember(p => p.ID, a => a.MapFrom(o => o.ID))
                .ForMember(p => p.DateCreated, a => a.MapFrom(o => o.DateCreated == null ? string.Empty : o.DateCreated.ToString("yyyy-MM-dd h:mm tt")))
                .ForMember(p => p.DateLastModified, a => a.MapFrom(o => o.DateLastModified == null ? string.Empty : o.DateLastModified.ToString("yyyy-MM-dd h:mm tt")))
                .ForMember(p => p.Title, a => a.MapFrom(o => o.Title))
                .ForMember(p => p.Text, a => a.MapFrom(o => o.Text))
                .ForMember(p => p.CreatedByUserID, a => a.MapFrom(o => o.CreatedByUserID))
                .ForMember(p => p.EditedByUserID, a => a.MapFrom(o => o.EditedByUserID))
                .ForMember(p => p.CreatedByUser, a => a.MapFrom(o => o.CreatedByUser))
                .ForMember(p => p.EditedByUser, a => a.MapFrom(o => o.EditedByUser));

            Mapper.CreateMap<Status, StatusJson>()
                .ForMember(p => p.ID, a => a.MapFrom(o => o.ID))
                .ForMember(p => p.Name, a => a.MapFrom(o => o.Name));

            Mapper.CreateMap<Project, ProjectJson>()
                .ForMember(p => p.ID, a => a.MapFrom(o => o.ID))
                .ForMember(p => p.DateCreated, a => a.MapFrom(o => o.DateCreated == null ? string.Empty : o.DateCreated.ToString("yyyy-MM-dd h:mm tt")))
                .ForMember(p => p.DateLastModified, a => a.MapFrom(o => o.DateLastModified == null ? string.Empty : o.DateLastModified.ToString("yyyy-MM-dd h:mm tt")))
                .ForMember(p => p.Name, a => a.MapFrom(o => o.Name));

        }

        private static string FromBase64(string input)
        {
            input = input.TrimEnd('=');
            var decodedBytes = Convert.FromBase64String(input);
            return Encoding.UTF8.GetString(decodedBytes);
        }

        private static string ToBase64(string input)
        {
            byte[] encodedBytes = Encoding.UTF8.GetBytes(input);
            string encodedText = Convert.ToBase64String(encodedBytes);
            encodedText = encodedText.PadRight(encodedText.Length + (4 - encodedText.Length % 4) % 4, '=');
            return encodedText;
        }
    }
}
