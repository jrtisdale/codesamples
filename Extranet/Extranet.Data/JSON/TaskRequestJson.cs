﻿using System.Collections.Generic;
using AutoMapper;
using Extranet.Data.Models;

namespace Extranet.Data.JSON
{
    public class TaskRequestJson
    {
        public TaskRequestJson()
        {
            Comments = new List<CommentJson>();
        }

        public static TaskRequestJson MapFrom(TaskRequest toConvert)
        {
            return Mapper.Map<TaskRequestJson>(toConvert);
        }

        public string ID { get; set; }
        public string DateCreated { get; set; }
        public string DateLastModified { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public string DateDue { get; set; }
        public string CreatedByUserID { get; set; }
        public string AssignedToUserID { get; set; }
        public string StatusID { get; set; }
        public string IsComplete { get; set; }

        public UserJson CreatedByUser { get; set; }
        public UserJson AssignedToUser { get; set; }
        public ICollection<CommentJson> Comments { get; set; }
        public StatusJson Status { get; set; }
        public ProjectJson Project { get; set; }
        public string TimeString { get; set; }

        public string ProjectID { get; set; }
    }
}