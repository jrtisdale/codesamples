using System.Collections.Generic;
using AutoMapper;
using Extranet.Data.Models;

namespace Extranet.Data.JSON
{
    public class UserJson
    {
        public UserJson()
        {
            this.CreatedComments = new List<CommentJson>();
            this.EditedComments = new List<CommentJson>();
            this.AssignedTasks = new List<TaskRequestJson>();
            this.CreatedTasks = new List<TaskRequestJson>();
            //this.Roles = new List<RoleJson>();
        }

        public static UserJson MapFrom(User toConvert)
        {
            return Mapper.Map<UserJson>(toConvert);
        }

        public string ID { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public virtual ICollection<CommentJson> CreatedComments { get; set; }
        public virtual ICollection<CommentJson> EditedComments { get; set; }
        public virtual ICollection<TaskRequestJson> AssignedTasks { get; set; }
        public virtual ICollection<TaskRequestJson> CreatedTasks { get; set; }
        //public virtual ICollection<RoleJson> Roles { get; set; }
    }
}