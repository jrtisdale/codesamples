using AutoMapper;
using Extranet.Data.Models;

namespace Extranet.Data.JSON
{
    public class CommentJson
    {
        public CommentJson()
        {
            
        }

        public static CommentJson MapFrom(Comment toConvert)
        {
            return Mapper.Map<CommentJson>(toConvert);
        }

        public string ID { get; set; }
        public string DateCreated { get; set; }
        public string DateLastModified { get; set; }

        public int? CreatedByUserID { get; set; }
        public int? EditedByUserID { get; set; }

        public string Title { get; set; }
        public string Text { get; set; }

        public virtual UserJson CreatedByUser { get; set; }
        public virtual UserJson EditedByUser { get; set; }
    }
}