using AutoMapper;
using Extranet.Data.Models;

namespace Extranet.Data.JSON
{
    public class ProjectJson
    {
        public ProjectJson()
        {
            //TaskRequests = new List<TaskRequestJson>();
        }

        public static ProjectJson MapFrom(Project toConvert)
        {
            return Mapper.Map<ProjectJson>(toConvert);
        }

        public string ID { get; set; }
        public string DateCreated { get; set; }
        public string DateLastModified { get; set; }
        public string Name { get; set; }
    }
}