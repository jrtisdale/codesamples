namespace Extranet.Data.JSON
{
    public class StatusJson
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
}