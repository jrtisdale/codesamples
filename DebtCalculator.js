﻿/*
    Sample address: http://budgetfuldev.azurewebsites.net/
*/

function DebtCalculator(debts, netIncome, nonDebtExpenses) {
    this.Debts = debts;
    this.NetIncome = netIncome;
    this.NonDebtExpenses = nonDebtExpenses;
    this.CurrentMonth = 0;
    this.TotalInterestPaid = 0;
    this.TotalAmountPaid = 0;
}

DebtCalculator.prototype = {
    calculate: function() {
        this.resetCurrents();
        
        if ((this.NetIncome - this.NonDebtExpenses - this.sumOfAllMinimums() - this.sumOfAllInterest()) <= 0) {
            return {
                "Error": "You will never pay off this amount of debt at this income level."
            };
        }
        
        while (this.getNextUnpaid() != null) {
            this.advanceMonth();
        }

        return this;
    },
    advanceMonth: function() {
        var budgetRemaining = this.NetIncome - this.NonDebtExpenses;

        // pay minimum payments
        for (var debtIndex in this.Debts) {
            var debt = this.Debts[debtIndex];

            if (debt.CurrentAmount <= 0) {
                continue;
            }

            var interest = this.calculateInterest(debt);
            this.pay(debt, -interest, "Accrued Interest");
            this.TotalInterestPaid += interest;

            var min = (debt.CurrentAmount - debt.MinimumPayment >= 0) ? debt.MinimumPayment : debt.CurrentAmount;
            
            this.pay(debt, min, "Minimum payment for month " + this.CurrentMonth);

            budgetRemaining -= min;
            this.TotalAmountPaid += min;
        }

        // get the next unpaid debt
        var nextUnpaid = this.getNextUnpaid();
        
        while (budgetRemaining > 0 && nextUnpaid != null) {
            // push any extra into an extra payment on the first debt.
            // continue until we run out of money or debts to pay

            var payment = (nextUnpaid.CurrentAmount - budgetRemaining <= 0) ? nextUnpaid.CurrentAmount : budgetRemaining;

            this.pay(nextUnpaid, payment, "Extra principal payment");
            budgetRemaining -= payment;
            this.TotalAmountPaid += payment;

            nextUnpaid = this.getNextUnpaid();
        }

        if(this.getNextUnpaid())
            this.CurrentMonth++;
    },
    resetCurrents: function () {
        this.CurrentMonth = 1;
        this.Payments = new Array(0);
        for (var debtIndex in this.Debts) {
            var debt = this.Debts[debtIndex];
            debt.CurrentAmount = debt.StartingAmount;
            debt.PaidOffOn = 0;
            debt.Payments = new Array(0);
        }
    },
    pay: function(debt, amount, comment) {
        if (debt.CurrentAmount <= 0) return;

        debt.CurrentAmount -= amount;
        this.Payments.push({ "Month": this.CurrentMonth, "Amount": -amount, "DebtName": debt.Name, "DebtCurrentTotal": debt.CurrentAmount, "Comment": comment });
        debt.Payments.push({ "Month": this.CurrentMonth, "Amount": -amount, "DebtName": debt.Name, "DebtCurrentTotal": debt.CurrentAmount, "Comment": comment });
        
        if (debt.CurrentAmount <= 0) {
            debt.PaidOffOn = this.CurrentMonth;
        }
    },
    sumOfAllMinimums: function() {
        // sum of minimum payments
        var sum = 0;
        for (var debtIndex in this.Debts) {
            if (debtIndex.CurrentAmount > 0) {
                sum += debtIndex.MinimumPayment;
            }
        }
        return sum;
    },
    sumOfAllInterest: function() {
        // sum of interest that will be accrued this month
        var sum = 0;
        for (var debtIndex in this.Debts) {
            sum += this.calculateInterest(this.Debts[debtIndex]);
        }
        return sum;
    },
    calculateInterest: function(forDebt) {
        // naive interest calculation, by month
        if (forDebt.CurrentAmount <= 0 || forDebt.InterestRate <= 0) {
            return 0.0;
        }
        var interest = ((forDebt.InterestRate / 12) * forDebt.CurrentAmount).toFixed(2);
        return parseFloat(interest);
    },
    countUnpaid: function() {
        // how many unpaid debts are left?
        var unpaid = 0;
        for (var debtIndex in this.Debts) {
            var debt = this.Debts[debtIndex];
            if (debt.CurrentAmount > 0) {
                unpaid++;
            }
        }
        return unpaid;
    },
    getNextUnpaid: function() {
        var unpaid;
        for (var debtIndex in this.Debts) {
            var debt = this.Debts[debtIndex];
            if (debt.CurrentAmount > 0) {
                unpaid = debt;
                break;
            }
        }
        return unpaid;
    },
    sortForSnowball: function() {
        // Sort by DebtAmount, ascending
        this.Debts.sort(function(a, b) {
            return a.StartingAmount - b.StartingAmount;
        });
    },
    sortForHIF: function() {
        // Sort by interest rate, descending
        this.Debts.sort(function(a, b) {
            return b.InterestRate - a.InterestRate;
        });
    }
}